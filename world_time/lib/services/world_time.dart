import 'package:http/http.dart';
import 'dart:convert';
import 'package:intl/intl.dart';

class WorldTime {
  String location; // location name for the UI
  late String time; // the time in the location
  String flag; // url to the flag asset
  String url; // location url for api endpoint
  late bool isDaytime; // treu or false if daytime or not

  WorldTime({required this.location, required this.flag, required this.url});

  Future<void> fetchTime() async {
    try {
      // Request to WorldTime API
      Response response =
          await get(Uri.parse('http://worldtimeapi.org/api/timezone/$url'));
      Map data = jsonDecode(response.body);

      // get properties from data
      String datetime = data['datetime'];
      String offset = data['utc_offset'].substring(0, 3);

      // generate DateTime object with timezone offset
      DateTime now = DateTime.parse(datetime);
      now = now.add(Duration(hours: int.parse(offset)));

      time = DateFormat.jm().format(now);
      isDaytime = now.hour > 6 && now.hour < 18 ? true : false;
    } catch (e) {
      print("Caught error: $e");
      time = "Could not get time data";
    }
  }
}
