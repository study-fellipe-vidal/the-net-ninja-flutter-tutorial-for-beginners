import 'package:flutter/material.dart';
import 'package:world_time/screens/choose_location.screen.dart';
import 'package:world_time/screens/home.screen.dart';
import 'package:world_time/screens/loading.screen.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: '/',
    routes: {
      '/': (context) => const LoadingScreen(),
      '/home': (context) => const HomeScreen(),
      '/location': (context) => const ChooseLocationScreen()
    },
  ));
}
